import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';

class OrderStatusPage extends StatefulWidget {
  final double srcLatitude;
  final double srcLongitude;
  final double destLatitude;
  final double destLongitude;
  const OrderStatusPage({
    Key key,
    this.srcLatitude = 42.7477863,
    this.srcLongitude = -71.1699932,
    this.destLatitude = 42.6871386,
    this.destLongitude = -71.2143403,
  }) : super(key: key);

  @override
  _OrderStatusPageState createState() => _OrderStatusPageState();
}

class _OrderStatusPageState extends State<OrderStatusPage> {
  final Completer<GoogleMapController> _controller = Completer();
  GoogleMapController mapController;
  final Set<Marker> _markers = {};
  final Set<Polyline> _polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPIKey = "AIzaSyDQNxk3MlH7a0GPBLVsdsdf2ghtyeict1U";
  BitmapDescriptor sourceIcon;
  BitmapDescriptor destinationIcon;

  @override
  void initState() {
    super.initState();
    setSourceAndDestinationIcons();
  }

  Future<void> setSourceAndDestinationIcons() async {
    sourceIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(20.0, 20.0)),
        'assets/icons/map_home.png');
    destinationIcon = await BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(size: Size(20.0, 20.0)),
        'assets/icons/map_food.png');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.white.withOpacity(0),
        elevation: 0.0,
        leading: IconButton(
          icon: Icon(
            Icons.navigate_before,
            size: 20.0,
            color: Colors.grey,
          ),
          onPressed: () {
            // ExtendedNavigator.of(context).pop();
          },
        ),
      ),
      body: GoogleMap(
        myLocationEnabled: true,
        zoomControlsEnabled: false,
        myLocationButtonEnabled: false,
        initialCameraPosition: CameraPosition(
          zoom: 14,
          bearing: 30,
          target: LatLng(
            widget.srcLatitude,
            widget.srcLongitude,
          ),
        ),
        onMapCreated: _onMapCreated,
        markers: _markers,
        polylines: _polylines,
      ),
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    // controller.setMapStyle(Utils.mapStyles);
    _controller.complete(controller);
    setMapPins();
    setPolylines();

    LatLng temp;
    LatLng sourceLocation = LatLng(
      widget.srcLatitude,
      widget.srcLongitude,
    );

    LatLng destLocation = LatLng(
      widget.srcLatitude,
      widget.srcLongitude,
    );

    if (sourceLocation.latitude > destLocation.latitude) {
      temp = sourceLocation;
      sourceLocation = destLocation;
      destLocation = temp;
    }

    final LatLngBounds bound =
        LatLngBounds(southwest: sourceLocation, northeast: destLocation);

    final CameraUpdate u2 = CameraUpdate.newLatLngBounds(bound, 50);
    mapController.animateCamera(u2).then((void v) {
      check(u2, mapController);
    });
  }

  Future<void> check(CameraUpdate u, GoogleMapController c) async {
    c.animateCamera(u);
    mapController.animateCamera(u);
    final LatLngBounds l1 = await c.getVisibleRegion();
    final LatLngBounds l2 = await c.getVisibleRegion();

    if (l1.southwest.latitude == -90 || l2.southwest.latitude == -90) {
      check(u, c);
    } else {
      await setPolylines();
    }
  }

  Future<void> setPolylines() async {
    // final PolylineResult result =
    //     await polylinePoints.getRouteBetweenCoordinates(
    //   googleAPIKey,
    //   PointLatLng(
    //     widget.orderDet.sourceAddress.latitude,
    //     widget.orderDet.sourceAddress.longitude,
    //   ),
    //   PointLatLng(
    //     widget.orderDet.destAddress.latitude,
    //     widget.orderDet.destAddress.longitude,
    //   ),
    //   travelMode: TravelMode.transit,
    // );

    // if (result.points.isNotEmpty) {
    //   result.points.forEach((PointLatLng point) {
    //     polylineCoordinates.add(LatLng(point.latitude, point.longitude));
    //   });
    // }

    polylineCoordinates.add(
      LatLng(
        widget.srcLatitude,
        widget.srcLongitude,
      ),
    );

    polylineCoordinates.add(
      LatLng(
        widget.destLatitude,
        widget.destLongitude,
      ),
    );

    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      final Polyline polyline = Polyline(
        polylineId: PolylineId("poly"),
        color: const Color.fromARGB(255, 40, 122, 198),
        patterns: [PatternItem.dash(30), PatternItem.gap(10)],
        width: 5,
        points: polylineCoordinates,
      );

      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
      _polylines.add(polyline);
    });
  }

  void setMapPins() {
    setState(
      () {
        // source pin
        _markers.add(
          Marker(
            markerId: MarkerId('sourcePin'),
            position: LatLng(
              widget.srcLatitude,
              widget.srcLongitude,
            ),
            icon: sourceIcon,
          ),
        );
        // destination pin
        _markers.add(
          Marker(
            markerId: MarkerId('destPin'),
            position: LatLng(
              widget.destLatitude,
              widget.destLongitude,
            ),
            icon: destinationIcon,
          ),
        );
      },
    );
  }
}

class Utils {
  static String mapStyles = '''[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "elementType": "labels.icon",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#f5f5f5"
      }
    ]
  },
  {
    "featureType": "administrative.land_parcel",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#bdbdbd"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#ffffff"
      }
    ]
  },
  {
    "featureType": "road.arterial",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#757575"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#dadada"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#616161"
      }
    ]
  },
  {
    "featureType": "road.local",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  },
  {
    "featureType": "transit.line",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#e5e5e5"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#eeeeee"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#c9c9c9"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9e9e9e"
      }
    ]
  }
]''';
}
